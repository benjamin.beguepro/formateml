Definition : 
Format des messages électroniques (email), qui présente le contenu sous forme brut ; ce format supporte les pièces jointes.

Utilisation :
Générés par les programmes de messagerie tels que Microsoft Outlook ou Mozilla Thunderbird.

Risques : 
Les fichiers EML sont susceptibles d'être infectés par des virus et d'autres logiciels malveillants. 
Il est donc recommandé d'utiliser ce format avec parcimonie : Certains fichiers sous ce format, avec des noms 
dépourvus de sens font parfois l'objet d'une infection par le virus Nimda.

Constitution :
Les fichiers EML sont généralement des fichiers de texte brut. 
À côté de la zone de message qui correspond au corps de l'e-mail, se trouvent l'en-tête de l'e-mail et des liens joints. 
Le fichier EML peut également contenir des hyperliens et des pièces jointes.